const express = require("express");
const app = express();
const mysql = require("mysql");
const morgan = require("morgan");
const bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
const expressValidator = require("express-validator");
const fs = require("fs");
const cors = require("cors");
const dotenv = require("dotenv");
dotenv.config();

// const configuration = {
//     user: process.env.DATABASE_USER,
//     password: process.env.DATABASE_PASSWORD,
//     database: process.env.DATABASE_NAME
// }

// const port = process.env.ENV_PORT
// if (process.env.DATABASE_SOCKET) {
//     configuration.socketPath = process.env.DATABASE_SOCKET
// } else {
//     configuration.host = process.env.DATABASE_HOST
// }
// const conn = mysql.createConnection(configuration)

const db = mysql.createConnection({
  host: "localhost",
  user: "newuser",
  password: "#{2gaM7N{*6>}aTH",
  database: "portfolio",
  dateStrings: true,
});

// connect to database
db.connect((err) => {
  if (err) {
    throw err;
  }
  console.log("Connected to database");
});
global.db = db;

// bring in routes
const authRoutes = require("./routes/auth");
const userRoutes = require("./routes/user");

// middleware -
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(expressValidator());
app.use(cors());
app.use("/api", authRoutes);
app.use("/api", userRoutes);

app.get("/posts", function (request, result) {
  connection.connect();
  connection.query(
    "SELECT * FROM 'about_me';",
    function (err, results, fields) {
      if (err) throw err;
      result.send(results);
    }
  );
  connection.end();
});

app.use(function (err, req, res, next) {
  if (err.name === "UnauthorizedError") {
    res.status(401).json({ error: "Unauthorized!" });
  }
});

// App Server Connection
app.listen(process.env.ENV_PORT || 8000, () => {
  console.log(`app is running on port ${process.env.PORT || 8000}`);
});
