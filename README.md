
## TO RUN

1. Set up MySQL by doing the following in the terminal or using Workbench

```
mysql -u root -p
enter password

USE mysql;
CREATE USER 'newuser'@'localhost' IDENTIFIED WITH mysql_native_password BY '#{2gaM7N{*6>}aTH';
GRANT ALL PRIVILEGES ON *.* TO 'newuser'@'localhost';
flush privileges;

CREATE DATABASE portfolio;
USE portfolio;
```




site url joelfabok.joelfabok.me