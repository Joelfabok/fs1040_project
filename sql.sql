



/* creates users */

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `_id` int unsigned NOT NULL AUTO_INCREMENT,
  `fName` varchar(30) NOT NULL,
  `lName` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` char(128) NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `role` varchar(30) NOT NULL DEFAULT 'subscriber'
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('35','test','test','test@test.com','57cc1a55de136151e1909db6a4d9726d0469b2bd3f1c4ace8fd0f188ed663452ab3b11a248ad896fb33411c9ce8798d62aac2cbc0bc354d6790f198efe74e94a','2021-05-03 10:43:20','subscriber'
);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

/* creates table for submissions from contact page */

DROP TABLE IF EXISTS `submissions`;

CREATE TABLE `submissions` (
  `id` int(10) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `fName` varchar(255) DEFAULT NULL,
  `lName` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
   `content` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



/* creates table for about me page */

DROP TABLE IF EXISTS `about_me`;

CREATE TABLE `about_me` (
  `about` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `about_me` VALUES ('Hi, my name is Joel, I have been working on websites for 5 years.
                I am currently working on getting my certificate in Full Stacks Development
                through York University.
                I enjoy helping non-profits and help companies market as well as update, maintain,
                and redesign their website so that they can be as affective as possible.
                If you would like
                to see some of the work that I have already done please check out the portfolio section of my site.');





/* creates table for portfolio page */
DROP TABLE IF EXISTS `portfolio`;

CREATE TABLE `portfolio` (
  `title` varchar(45) NOT NULL,
  `link` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `image` LONGBLOB NOT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
