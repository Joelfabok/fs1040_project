import React, { Component } from "react";
import { contact } from "../../auth";
import { Link } from "react-router-dom";

class Contact extends Component {
  constructor() {
    super();
    this.state = {
      fName: "",
      lName: "",
      email: "",
      address: "",
      content: "",
      error: "",
      open: false,
    };
  }

  handleChange = (name) => (event) => {
    this.setState({ error: "" });
    this.setState({ [name]: event.target.value });
  };

  clickSubmit = (event) => {
    event.preventDefault();
    const { fName, lName, email, address, content } = this.state;
    const user = {
      fName,
      lName,
      email,
      address,
      content,
    };
    // console.log(user);
    contact(user).then((data) => {
      if (data.error) this.setState({ error: data.error });
      else
        this.setState({
          error: "",
          fName: "",
          lName: "",
          email: "",
          address: "",
          content: "",
          open: true,
        });
    });
  };

  resetForm = () => {
    this.setState({ error: "" });
  };

  contactForm = (fName, lName, email, address, content, recaptcha) => (
    <form method="P">
      <div className="form-group">
        <label className="text-muted">First Name</label>
        <input
          onChange={this.handleChange("fName")}
          onFocus={this.resetForm}
          type="text"
          className="form-control"
          value={fName}
        />
      </div>
      <div className="form-group">
        <label className="text-muted">Last Name</label>
        <input
          onChange={this.handleChange("lName")}
          onFocus={this.resetForm}
          type="text"
          className="form-control"
          value={lName}
        />
      </div>

      <div className="form-group">
        <label className="text-muted">Email</label>
        <input
          onChange={this.handleChange("email")}
          onFocus={this.resetForm}
          type="email"
          className="form-control"
          value={email}
        />
      </div>
      <div className="form-group">
        <label className="text-muted">Address</label>
        <input
          onChange={this.handleChange("address")}
          onFocus={this.resetForm}
          type="text"
          className="form-control"
          value={address}
        />
      </div>

      <div className="form-group">
        <label className="text-muted">Message</label>
        <input
          onChange={this.handleChange("content")}
          onFocus={this.resetForm}
          type="text"
          className="form-control"
          value={content}
        />
      </div>

      <button
        onClick={this.clickSubmit}
        className="btn btn-raised formSubmitButton"
      >
        Submit
      </button>
    </form>
  );

  render() {
    const { fName, lName, email, address, content, error } = this.state;
    return (
      <div className="container sign-up ">
        <h2 className="mt-5 mb-5">Contact Us</h2>

        <div
          className="alert alert-danger"
          style={{ display: error ? "" : "none" }}
        >
          {error}
        </div>

        {this.contactForm(fName, lName, email, address, content)}
      </div>
    );
  }
}

export default Contact;
