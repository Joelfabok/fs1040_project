import { BrowserRouter } from "react-router-dom";
import MainRouter from "./MainRouter";
import '../src/components/css/home.css'
import '../src/components/css/about.css'
import '../src/components/css/index.css'

function App() {
  return (

    <BrowserRouter>
    <MainRouter />
    </BrowserRouter>
  );
}

export default App;
