import React, { Fragment } from "react";
// import { Link, withRouter } from "react-router-dom";
// ADDED NavLink with exact and activeClassName="active-link" below
import { NavLink, withRouter } from "react-router-dom";
import { signout, isAuthenticated } from "../auth";
import "./../App.css";
import { Nav, Navbar } from "react-bootstrap";
import logo from "../components/images/Logo.png";

const isActive = (history, path) => {
  if (history.location.pathname === path) return { color: "#ff9900" };
  else return { color: "#ffffff" };
};

const Menu = ({ history }) => {
  const user = isAuthenticated().user;
  console.log("user", user);
  return (
    <>
      <Navbar
        collapseOnSelect
        expand="lg"
        className="bg-custom-4 py-0 "
        variant="dark"
      >
        <Navbar.Brand href="/">
          <img
            className="logo"
            src={logo}
            width={50}
            height={50}
            alt="Talens General Hospital Logo"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <li className="nav-item">
              <NavLink
                exact
                activeClassName="active-link"
                className="nav-link"
                style={isActive(history, "/")}
                to="/"
              >
                Home
              </NavLink>
              <NavLink
                exact
                activeClassName="active-link"
                className="nav-link"
                style={isActive(history, "/")}
                to="/about"
              >
                About
              </NavLink>
              <NavLink
                exact
                activeClassName="active-link"
                className="nav-link"
                style={isActive(history, "/")}
                to="/portfolio"
              >
                Portfolio
              </NavLink>
              <NavLink
                exact
                activeClassName="active-link"
                className="nav-link"
                style={isActive(history, "/")}
                to="/contact"
              >
                Contact
              </NavLink>
            </li>

            {!user && (
              <React.Fragment>
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    activeClassName="active-link"
                    style={isActive(history, "/signin")}
                    to="/signin"
                  >
                    Sign In
                  </NavLink>
                </li>
              </React.Fragment>
            )}

            {user && (
              <React.Fragment>
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    activeClassName="active-link"
                    style={isActive(history, "/signup")}
                    to="/signup"
                  >
                    Sign Up
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    exact
                    activeClassName="active-link"
                    className="nav-link"
                    style={isActive(history, "/")}
                    to="/editabout"
                  >
                    Edit About
                  </NavLink>
                </li>

                <li className="nav-item">
                  <NavLink
                    exact
                    activeClassName="active-link"
                    className="nav-link"
                    style={isActive(history, "/")}
                    to="/editportfolio"
                  >
                    Edit Portfolio
                  </NavLink>
                </li>

                <li className="nav-item">
                  <span
                    className="nav-link"
                    activeClassName="active-link"
                    style={{ cursor: "pointer", color: "#fff" }}
                    onClick={() => signout(() => history.push("/"))}
                  >
                    {`${isAuthenticated().user.fName} `}
                    Sign Out
                  </span>
                </li>
              </React.Fragment>
            )}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};

export default withRouter(Menu);
