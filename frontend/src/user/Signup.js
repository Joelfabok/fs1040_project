import React, { Component } from "react";
import { signup } from "../auth";
import { Link } from "react-router-dom";


class Signup extends Component {
	constructor() {
		super();
		this.state = {
			fName: "",
			lName: "",
			email: "",
			password: "",
			error: "",
			open: false,
		};
	}

	handleChange = (name) => (event) => {
		this.setState({ error: "" });
		this.setState({ [name]: event.target.value });
	};

	clickSubmit = (event) => {
		event.preventDefault();
		const { fName, lName, email, password } = this.state;
		const user = {
			fName,
			lName,
			email,
			password,
		};
		// console.log(user);
		signup(user).then((data) => {
			if (data.error) this.setState({ error: data.error });
			else
				this.setState({
					error: "",
					fName: "",
					lName: '',
					email: "",
					password: "",
					open: true,
				});
		});
	};

	resetForm = () => {
		this.setState({ error: ""});
	};

	signupForm = (fName, lName, email, password, recaptcha) => (
		<form>
			<div className="form-group">
				<label className="text-muted">First Name</label>
				<input
					onChange={this.handleChange("fName")}
					onFocus={this.resetForm}
					type="text"
					className="form-control"
					value={fName}
				/>
			</div>
						<div className="form-group">
				<label className="text-muted">Last Name</label>
				<input
					onChange={this.handleChange("lName")}
					onFocus={this.resetForm}
					type="text"
					className="form-control"
					value={lName}
				/>
			</div>

			<div className="form-group">
				<label className="text-muted">Email</label>
				<input
					onChange={this.handleChange("email")}
					onFocus={this.resetForm}
					type="email"
					className="form-control"
					value={email}
				/>
			</div>
			<div className="form-group">
				<label className="text-muted">Password</label>
				<input
					onChange={this.handleChange("password")}
					onFocus={this.resetForm}
					type="password"
					className="form-control"
					value={password}
				/>
			</div>

			<button onClick={this.clickSubmit} className="btn btn-raised formSubmitButton">
				Submit
			</button>
		</form>
	);

	render() {
		const { fName, lName, email, password, error, open } = this.state;
		return (
			<div className="container sign-up">
				<h2 className="mt-5 mb-5">Sign Up</h2>

				{/* <hr />
				<br /> */}

				<div
					className="alert alert-danger"
					style={{ display: error ? "" : "none" }}
				>
					{error}
				</div>

				<div
					className="alert alert-info"
					style={{ display: open ? "" : "none" }}
				>
					New account is successfully created. Please{" "}
					<Link to="/signin">Sign In</Link>.
				</div>

				{this.signupForm(fName, lName, email, password)}
			</div>
		);
	}
}

export default Signup;
