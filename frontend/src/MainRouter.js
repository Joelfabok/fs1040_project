import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./core/Home";
import Menu from "./core/Menu";
import Signup from "./user/Signup";
import Signin from "./user/Signin";
import Users from "./user/Users";
import EditProfile from "./user/EditProfile";
//import EditRecord from "./record/EditRecord";
import PrivateRoute from "./auth/PrivateRoute";
import Contact from "./components/pages/Contact";
import About from "./components/pages/About";
import Portfolio from "./components/pages/Portfolio";
import EditAbout from "./components/pages/EditAbout";
import EditPortfolio from "./components/pages/EditPortfolio";
import Footer from "./core/Footer";

const MainRouter = () => (
  <div>
    <Menu />
    <Switch>
      <Route exact path="/" component={Home} />
      {/* <PrivateRoute exact path="/admin" component={Admin} /> */}
      <Route exact path="/portfolio" component={Portfolio} />
      <Route exact path="/about" component={About} />
      <Route exact path="/contact" component={Contact} />
      <Route exact path="/editabout" component={EditAbout} />
      <Route exact path="/editportfolio" component={EditPortfolio} />

      {/* <Route exact path="/patient/:patientId" component={SinglePatient} />
			<PrivateRoute
				exact
				path="/patient/edit/:patientId"
				component={EditPatient}
			/> */}
      {/* <Route exact path="/record/:recordId" component={SingleRecord} />
			<Route exact path="/record/edit/:recordId" component={EditRecord} /> */}

      <Route exact path="/users" component={Users} />
      <Route exact path="/signup" component={Signup} />
      <Route exact path="/signin" component={Signin} />
      <PrivateRoute exact path="/user/edit/:userId" component={EditProfile} />
    </Switch>
  </div>
);

export default MainRouter;
